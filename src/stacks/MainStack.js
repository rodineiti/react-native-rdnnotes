import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import ListScreen from "./../screens/ListScreen";
import AddScreen from "./../screens/AddScreen";

const Stack = createStackNavigator();

export default function MainStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen name="List" component={ListScreen} />
      <Stack.Screen name="AddNote" component={AddScreen} />
    </Stack.Navigator>
  );
}

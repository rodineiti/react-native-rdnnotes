import styled from "styled-components/native";

export const Container = styled.View`
  flex:1;
  background-color: #333;
`

export const TitleInput = styled.TextInput`
  font-size: 20px;
  font-weight: bold;
  padding: 15px;
  color:#fff;
`

export const BodyInput = styled.TextInput`
  flex: 1;
  font-size: 15px;
  padding: 15px;
  color:#fff;
`

export const ButtonSave = styled.TouchableHighlight`
  margin-right: 20px;
`;

export const ButtonClose = styled.TouchableHighlight`
  margin-left: 15px;
`;

export const ButtonDelete = styled.TouchableHighlight`
  justify-content: center;
  align-items: center;
  background-color: #ff3333;
  height: 60px;
`;

export const ButtonDeleteTitle = styled.Text`
  color: #fff;
  font-size: 16px;
`;
import React, { useState, useEffect, useLayoutEffect } from "react";
import { Alert } from "react-native";
import { Fontisto, AntDesign } from '@expo/vector-icons';
import { useSelector, useDispatch } from "react-redux";
import { Container, TitleInput, BodyInput, ButtonSave, ButtonClose, ButtonDelete, ButtonDeleteTitle } from "./styled";

export default function AddScreen({ route, navigation }) {
  const dispatch = useDispatch();
  const list = useSelector(state => state.notesReducer.list);

  const [title, setTitle] = useState("");
  const [body, setBody] = useState("");
  const [edit, setEdit] = useState(false);

  const handleSave = () => {
    if (title !== "" && body !== "") {
      if (edit) {
        dispatch({
          type: "EDIT_NOTE",
          payload: { key: route.params.key, title, body }
        });
      } else {
        dispatch({
          type: "STORE_NOTE",
          payload: { title, body }
        });
      }
      navigation.goBack();
    } else {
      alert("Preencha os campos título e conteúdo");
    }
  }

  const handleClose = () => {
    navigation.goBack();
  }

  const handleDestroy = () => {
    if (route.params?.key !== undefined && list[route.params.key]) {
      Alert.alert(
        "ATENÇÃO",
        "Deseja realmente ecluir?",
        [
          {
            text: "Cancelar",
            onPress: () => console.log("Cancel Pressed"),
            style: "cancel"
          },
          { text: "Sim", onPress: () => {
            dispatch({
              type: "DESTROY_NOTE",
              payload: { key: route.params.key }
            });
            navigation.goBack();
          } }
        ],
        { cancelable: false }
      );
    }
  }

  useLayoutEffect(() => {
    navigation.setOptions({
      title: !edit ? "Nova anotação" : "Editar anotação",
      headerLeft: () => (
        <ButtonClose underlayColor="transparent" onPress={handleClose}>
          <AntDesign name="close" size={28} color="black" />
        </ButtonClose>
      ),
      headerRight: () => (
        <ButtonSave underlayColor="transparent" onPress={handleSave}>
          <Fontisto name="save" size={24} color="black" />
        </ButtonSave>
      )
    })
  }, [edit, title, body]);

  useEffect(() => {
    // set not required route.params?
    if (route.params?.key !== undefined && list[route.params.key]) {
      setEdit(true);
      setTitle(list[route.params.key].title);
      setBody(list[route.params.key].body);
    }
  }, []);

  return (
    <Container>
      <TitleInput 
        value={title}
        onChangeText={text => setTitle(text)}
        placeholder="Digite o título" 
        placeholderTextColor="#ccc" autoFocus={false} />

      <BodyInput 
        value={body}
        onChangeText={text => setBody(text)}
        placeholder="Digite o conteúdo" 
        placeholderTextColor="#ccc" 
        multiline={true} />

      {edit && (
        <ButtonDelete underlayColor="transparent" onPress={handleDestroy}>
          <ButtonDeleteTitle>Excluir anotação</ButtonDeleteTitle>
        </ButtonDelete>
      )}
    </Container>
  )
}
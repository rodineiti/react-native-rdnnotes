import React, { useLayoutEffect } from "react";
import { useSelector } from "react-redux";
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import { Container, AddButton, List, Empty, EmptyDescription } from "./styled";
import Item from "../../components/Item";

export default function ListScreen({ navigation }) {
  const list = useSelector(state => state.notesReducer.list);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: "Suas notas",
      headerRight: () => (
        <AddButton underlayColor="transparent" onPress={() => navigation.navigate("AddNote")}>
          <Ionicons name="md-add" size={32} color="black" />
        </AddButton>
      )
    })
  }, []);

  const handlePress = (index) => {
    navigation.navigate("AddNote", { key: index });
  }

  return (
    <Container>
      {list.length > 0 ? (
        <List
          data={list}
          renderItem={({item, index}) => (
            <Item data={item} index={index} onPress={handlePress} />
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      ) : (
        <Empty>
          <MaterialIcons name="event-note" size={48} color="white" />
          <EmptyDescription>Nenhuma anotação</EmptyDescription>
        </Empty>
      )}
    </Container>
  )
}
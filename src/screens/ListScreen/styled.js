import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  background-color: #333;
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  color: #fff;
`;

export const AddButton = styled.TouchableHighlight`
  margin-right: 20px;
`;

export const List = styled.FlatList`
  width: 100%;
`;

export const Empty = styled.View`
  justify-content: center;
  align-items: center;
`;

export const EmptyDescription = styled.Text`
  color: #fff;
  font-size: 20px;
  margin-top: 10px;
`;
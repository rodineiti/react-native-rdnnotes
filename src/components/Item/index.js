import React from "react";

import { Container, Title } from "./styled";

export default function Item({ data, index, onPress }) {
  return (
    <Container onPress={() => onPress(index)}>
      <Title>{data.title || ""}</Title>
    </Container>
  )
}
const initialState = {
  list: [
    {
      title: "Item 1",
      body: "Teste body 1"
    },
    {
      title: "Item 2",
      body: "Teste body 2"
    }
  ]
};

export default (state = initialState, action) => {
  let newList = [...state.list];
  switch (action.type) {
    case "STORE_NOTE":
      newList.push({
        title: action.payload.title,
        body: action.payload.body
      });
      return { ...state, list: newList };
    case "EDIT_NOTE":
      if (newList[action.payload.key]) {
        newList[action.payload.key] = { title: action.payload.title, body: action.payload.body };
      }
      return { ...state, list: newList };
    case "DESTROY_NOTE":
      if (newList[action.payload.key]) {
        newList = newList.filter((item, index) => index !== action.payload.key);
      }
      return { ...state, list: newList };
    default:
      return state;
  }
};